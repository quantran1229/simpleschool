'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('Classes','teacherId',{
        type: Sequelize.INTEGER,
        onDelete:"CASCADE",
        references:{
          model:'Teachers',
          key:'id',
          as: 'teacherId'
        }
      });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('Classes', 'teacherId')
  }
};
