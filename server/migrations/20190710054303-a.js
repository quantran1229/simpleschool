module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('Units', 'test_num', Sequelize.INTEGER);
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('Units', 'test_num');
  }
};
