const units = require('./unit');
const students = require('./student');
const teachers = require('./teacher');
const classes = require('./class');
const search = require('./search');

module.exports = {
  units,
  classes,
  teachers,
  students,
  search,
}
