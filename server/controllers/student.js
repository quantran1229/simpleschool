const Student = require('../models').Student;
const Unit = require('../models').Unit;
const Sequelize = require('sequelize');

function validScore(i)
{
  if (!i && i != 0) return false;
  if (!Number.isInteger(i)) return false;
  if (i < 0 || i > 10) return false;
  return true;
}

module.exports = {
  create(req, res) {
    return Student
      .create({
        firstname: req.body.fname,
        lastname: req.body.lname,
        dob: req.body.dob,
        classId:req.body.classId,
        marks:req.body.marks,
      })
      .then(student => res.status(201).send(student))
      .catch(error => res.status(400).send(error));
  },
  list(req, res) {
    return Student
      .findAll()
      .then((students) => res.status(200).send(students))
      .catch((error) => res.status(400).send(error));
  },
  retrieve(req,res){
    return Student
      .findByPk(req.params.studentId,{
      })
      .then(student =>{
        if (!student)
        {
          return res.status(404).send({message:"Student not found"});
        }
        return res.status(200).send(student);
      })
      .catch(error => teacherIdresUnit.count().then(c => {
      })
      .catch(error => res.status(400).send(error)).status(400).send(error));
  },
  update(req,res){
    return Student
    .findByPk(req.params.studentId)
    .then(student =>{
      if (!student)
      {
        return res.status(404).send({message:"Student not found"});
      }
      var reqmarks = req.body.marks || "{}";
      reqmarks = JSON.parse(reqmarks);
      Unit.count().then(c => {
        var i;
        var newmarks = {};
        var oldmarks = student.marks;
        for (i = 1;i<=c;i++)
        {
          var key = i.toString();
          if (validScore(reqmarks[key]))
          {
            newmarks[key] = reqmarks[key];
          }
          else {
            if (validScore(oldmarks[key]))
            {
              newmarks[key] = oldmarks[key];
            }
            else {
              newmarks[key] = -1;
            }
          }
        }
        return student.update({
          name: req.body.name || student.name,
          firstname: req.body.fname || student.firstname,
          lastname: req.body.lname || student.lastname,
          dob: req.body.dob || student.dob,
          marks:newmarks,
        })
        .then(()=> res.status(200).send(student))
        .catch((error)=>res.status(400).send(error));
      })
      .catch(error => res.status(400).send(error));;
    })
    .catch(error => res.status(400).send(error));
  },
  delete(req,res)
  {
    return Student
    .findByPk(req.params.studentId)
    .then(student =>{
      if (!student)
      {
        return res.status(404).send({message:"Student not found"});
      }
      return student.destroy()
      .then(()=> res.status(200).send({message:"This Student has been destroy"}))
      .catch((error)=>res.status(400).send(error));
    })
    .catch(error => res.status(400).send(error));
  },
  deleteAll(req,res)
  {
    return Student.destroy({
      where:{},
      truncate:false
    })
    .then(()=> {res.status(200).send({message:"All Student records has been destroy"});})
    .catch((error)=>res.status(400).send(error));
  }
};
