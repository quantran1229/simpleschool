const Student = require('../models').Student;
const Class = require('../models').Class;
const Teacher = require('../models').Teacher;
const Unit = require('../models').Unit;
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
module.exports = {
  students(req, res) {
    return Student
      .findAll({
        where:
        {
          [Op.or]:[{
            firstname:{[Op.iLike]:"%"+req.query.name+"%"}
          },{
            lastname:{[Op.iLike]:"%"+req.query.name+"%"}
          }]
        },
      })
      .then(student => res.status(201).send(student))
      .catch(error => res.status(400).send(error));
  },
  teachers(req, res) {
    return Teacher
      .findAll({
        where:
        {
          [Op.or]:[{
            firstname:{[Op.iLike]:"%"+req.query.name+"%"}
          },{
            lastname:{[Op.iLike]:"%"+req.query.name+"%"}
          }]
        },
      })
      .then(teacmhers => res.status(201).send(teachers))
      .catch(error => res.status(400).send(error));
  },
  units(req, res) {
    return Unit
      .findAll({
        where:
        {
          [Op.or]:[{
            name:{[Op.iLike]:req.query.name}
          },{
            description:{[Op.iLike]:"%"+req.query.name+"%"}
          }]
        },
      })
      .then(units => res.status(201).send(units))
      .catch(error => res.status(400).send(error));
  },
  score(req,res)
  {
    console.log(req.query);
    var unit_id = req.query.unit || 1;
    console.log(unit_id);
    let unit_name = null;
    Unit.findByPk(unit_id,{}).then(unit =>{
      if (!unit)
      {
        return res.status(404).send({message:"Unit not found"});
      }
      unit_name = unit.name;
      var score = parseInt(req.query.score,10) || 0;
      return Student
      .findAll({
        attributes:["id","firstname","lastname",[Sequelize.json("marks."+[unit_id]),unit_name]],
        where:{
          marks:{
            [unit_id+"::integer"]:{
              [Op.gte]:score
            }
          }
        }
      })
      .then(students => res.status(200).send(students))
      .catch(error => res.status(400).send(error));
    })
    .catch(error => res.status(400).send(error));
  },
  test(req,res)
  {
    return Student.findAll({
      attributes:[Sequelize.fn("concat",Sequelize.col("firstname"),Sequelize.col("lastname"))],
    })
    .then(students => res.status(200).send(students))
    .catch(error => res.status(400).send(error));
  }
}
