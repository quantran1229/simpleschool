const Teacher = require('../models').Teacher;

module.exports = {
  create(req, res) {
    return Teacher
      .create({
        firstname: req.body.fname,
        lastname: req.body.lname,
        dob: req.body.dob,
        email: req.body.email,
        unitId:req.body.unitId
      })
      .then(teacher => res.status(201).send(teacher))
      .catch(error => res.status(400).send(error));
  },
  list(req, res) {
    return Teacher
      .findAll()
      .then((teachers) => res.status(200).send(teachers))
      .catch((error) => res.status(400).send(error));
  },
  retrieve(req,res){
    return Teacher
      .findByPk(req.params.teacherId,{})
      .then(teacher =>{
        if (!teacher)
        {
          return res.status(404).send({message:"Teacher not found"});
        }
        return res.status(200).send(teacher);
      })
      .catch(error => res.status(400).send(error));
  },
  update(req,res){
    return Teacher
    .findByPk(req.params.teacherId)
    .then(teacher =>{
      if (!teacher)
      {
        return res.status(404).send({message:"Teacher not found"});
      }
      return teacher.update({
        firstname: req.body.fname || teacher.firstname,
        lastname: req.body.lname || teacher.lastname,
        dob: req.body.dob || teacher.dob,
        email: req.body.email || teacher.email
      })
      .then(()=> res.status(200).send(teacher))
      .catch((error)=>res.status(400).send(error));
    })
    .catch(error => res.status(400).send(error));
  },
  delete(req,res)
  {
    return Teacher
    .findByPk(req.params.teacherId)
    .then(teacher =>{
      if (!teacher)
      {
        return res.status(404).send({message:"Teacher not found"});
      }
      return teacher.destroy()
      .then(()=> res.status(200).send({message:"This Teacher has been destroy"}))
      .catch((error)=>res.status(400).send(error));
    })
    .catch(error => res.status(400).send(error));
  },
  deleteAll(req,res)
  {
    return Teacher.destroy({
      where:{},
      truncate:false
    })
    .then(()=> {res.status(200).send({message:"All Teacher records has been destroy"});})
    .catch((error)=>res.status(400).send(error));
  }
};
