const Class = require('../models').Class;
const Student = require('../models').Student;
const Teacher = require('../models').Teacher;

module.exports = {
  create(req, res) {
    console.log(req.body.teacherId);
    return Class
      .create({
        name: req.body.name,
        teacherId:req.body.teacherId
      })
      .then(classes => res.status(201).send(classes))
      .catch(error => res.status(400).send(error));
  },
  list(req, res) {
    return Class
      .findAll({
        subQuery: false,
        include: [
          {
            model: Student,
            as: 'Students',
            attributes:['id','firstname','lastname']
          },
          {
            model:Teacher,
            attributes:['id','firstname','lastname']
          }
    ]})
      .then((classes) => res.status(200).send(classes))
      .catch((error) => res.status(400).send(error));
  },
  retrieve(req,res){
    return Class
      .findByPk(req.params.classId,{
        include: [{
        model: Student,
        as: 'Students',
        attributes:['id','firstname','lastname']
        }],
      })
      .then(classes =>{
        if (!classes)
        {
          return res.status(404).send({message:"Cattributes:['id','firstname','lastname']lass not found"});
        }
        return res.status(200).send(classes);
      })
      .catch(error => res.status(400).send(error));
  },
  update(req,res){
    return Class
    .findByPk(req.params.classId)
    .then(classes =>{
      if (!classes)
      {
        return res.status(404).send({message:"Class not found"});
      }
      return classes.update({
        name: req.body.name || classes.name,
        teacherId:req.body.teacherId || classes.teacherId
      })
      .then(()=> res.status(200).send(classes))
      .catch((error)=>res.status(400).send(error));
    })
    .catch(error => res.status(400).send(error));
  },
  delete(req,res)
  {
    return Class
    .findByPk(req.params.classId)
    .then(classes =>{
      if (!classes)
      {
        return res.status(404).send({message:"Class not found"});
      }
      return classes.destroy()
      .then(()=> res.status(200).send({message:"This Class has been destroy"}))
      .catch((error)=>res.status(400).send(error));
    })
    .catch(error => res.status(400).send(error));
  },
  deleteAll(req,res)
  {
    return Class.destroy({
      where:{},
      truncate:false
    })
    .then(()=> {res.status(200).send({message:"All Class records has been destroy"});})
    .catch((error)=>res.status(400).send(error));
  }
};
