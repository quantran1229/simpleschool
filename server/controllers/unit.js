const Unit = require('../models').Unit;
const Teacher = require('../models').Teacher;

module.exports = {
  create(req, res) {
    return Unit
      .create({
        name: req.body.name,
        description:req.body.des
      })
      .then(unit => res.status(201).send(unit))
      .catch(error => res.status(400).send(error));
  },
  list(req, res) {
    return Unit
      .findAll({
        include: [{
        model: Teacher,
        as: 'Teachers',
        attributes:['id','firstname','lastname']
        }],
      })
      .then((units) => res.status(200).send(units))
      .catch((error) => res.status(400).send(error));
  },
  retrieve(req,res){
    return Unit
      .findByPk(req.params.unitId,{
        include: [{
        model: Teacher,
        as: 'Teachers',
        attributes:['id','firstname','lastname']
        }],
      })
      .then(unit =>{
        if (!unit)
        {
          return res.status(404).send({message:"Unit not found"});
        }
        return res.status(200).send(unit);
      })
      .catch(error => res.status(400).send(error));
  },
  update(req,res){
    return Unit
    .findByPk(req.params.unitId)
    .then(unit =>{
      if (!unit)
      {
        return res.status(404).send({message:"Unit not found"});
      }
      return unit.update({
        name: req.body.name || unit.name,
        description:req.body.des || unit.description
      })
      .then(()=> res.status(200).send(unit))
      .catch((error)=>res.status(400).send(error));
    })
    .catch(error => res.status(400).send(error));
  },
  delete(req,res)
  {
    return Unit
    .findByPk(req.params.unitId)
    .then(unit =>{
      if (!unit)
      {
        return res.status(404).send({message:"unit not found"});
      }
      return unit.destroy()
      .then(()=> res.status(200).send({message:"This Unit has been destroy"}))
      .catch((error)=>res.status(400).send(error));
    })
    .catch(error => res.status(400).send(error));
  },
  deleteAll(req,res)
  {
    return Unit.destroy({
      where:{},
      truncate:false
    })
    .then(()=> {res.status(200).send({message:"All Unit records has been destroy"});})
    .catch((error)=>res.status(400).send(error));
  }
};
