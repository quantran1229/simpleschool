const Student = require('../models').Student;
const fs = require('fs');
const path = require('path');
const Sequelize = require('sequelize');
const basename = path.basename(module.filename);
const env = process.env.NODE_ENV || 'development';
const config = require(`${__dirname}/../config/config.json`)[env];
let sequelize;
if (config.use_env_variable) {
  sequelize = new Sequelize(process.env[config.use_env_variable]);
} else {
  sequelize = new Sequelize(
    config.database, config.username, config.password, config
  );
}

module.exports = {
  test(req,res)
  {
    return res.status(200).send({message:"OK"})
  },
  toLog()
  {
    sequelize.query("SELECT concat_ws(' ','firstname','lastname') FROM 'Students'",{type: sequelize.QueryTypes.SELECT })
    .then(users => {
      var i;
      for (i == 0;i < users.length;i++)
      {
        console.log(users[i].name);
      }
    })
  }
}
