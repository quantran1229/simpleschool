const unitsController = require('../controllers').units;
const classesController = require('../controllers').classes;
const studentsController = require('../controllers').students;
const teachersController = require('../controllers').teachers;
const searchController = require('../controllers').search;

module.exports = (app) => {
  app.get('/api', (req, res) => res.status(200).send({
    message: 'Welcome to the School API!',
  }));
  //for unit api
  app.post('/api/units', unitsController.create);
  app.get('/api/units', unitsController.list);
  app.get('/api/units/:unitId', unitsController.retrieve);
  app.put('/api/units/:unitId', unitsController.update);
  app.delete('/api/units/:unitId', unitsController.delete);

  //for classes API
  app.post('/api/classes', classesController.create);
  app.get('/api/classes', classesController.list);
  app.get('/api/classes/:classId', classesController.retrieve);
  app.put('/api/classes/:classId', classesController.update);
  app.delete('/api/classes/:classId', classesController.delete);

  //for student API
  app.post('/api/students', studentsController.create);
  app.get('/api/students', studentsController.list);
  app.get('/api/students/:studentId', studentsController.retrieve);
  app.put('/api/students/:studentId', studentsController.update);
  app.delete('/api/students/:studentId', studentsController.delete);

  //for teacher API
  app.post('/api/teachers', teachersController.create);
  app.get('/api/teachers', teachersController.list);
  app.get('/api/teachers/:teacherId', teachersController.retrieve);
  app.put('/api/teachers/:teacherId', teachersController.update);
  app.delete('/api/teachers/:teacherId', teachersController.delete);

  //test api
  app.get('/api/search/students', searchController.students);
  app.get('/api/search/teachers', searchController.teachers);
  app.get('/api/search/units', searchController.units);
  app.get('/api/search/score', searchController.score);
  app.get('/api/search/test',searchController.test);


  //remove API, only for testing
  app.delete('/api/remove/teachers',teachersController.deleteAll);
  app.delete('/api/remove/students',studentsController.deleteAll);
  app.delete('/api/remove/units',unitsController.deleteAll);
  app.delete('/api/remove/classes',classesController.deleteAll);


};
