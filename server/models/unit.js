module.exports = (sequelize, DataTypes) => {
  const Unit = sequelize.define('Unit', {
    name: {
      type:DataTypes.STRING
    },
    description: DataTypes.TEXT
  });

  Unit.associate = function(models) {
    // a unit has many teacher teach them
    Unit.hasMany(models.Teacher,{
      foreignKey:'unitId',
      as:'Teachers'
    });
  };
  return Unit;
};
