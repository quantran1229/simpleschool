module.exports = (sequelize, DataTypes) => {
  const Student = sequelize.define('Student', {
    firstname: DataTypes.STRING,
    lastname: DataTypes.STRING,
    dob: DataTypes.DATE,
    marks:DataTypes.JSONB,
  });
  Student.associate = function(models) {
    Student.belongsTo(models.Class,{
      foreignKey:'classId',
      onDelete:'CASCADE'
    });
  };
  return Student;
};
