module.exports = (sequelize, DataTypes) => {
  const Class = sequelize.define('Class', {
    name: DataTypes.STRING
  });
  Class.associate = function(models) {
    Class.hasMany(models.Student,{
      foreignKey:'classId',
      as:'Students'
    });
    Class.belongsTo(models.Teacher,{
      foreignKey:'teacherId',
      onDelete:'CASCADE'
    });
  };
  return Class;
};
