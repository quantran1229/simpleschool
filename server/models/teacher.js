module.exports = (sequelize, DataTypes) => {
  const Teacher = sequelize.define('Teacher', {
    firstname: DataTypes.STRING,
    lastname: DataTypes.STRING,
    dob: DataTypes.DATE,
    email: DataTypes.STRING
  });
  Teacher.associate = function(models) {
    Teacher.belongsTo(models.Unit,{
      foreignKey:'unitId',
      onDelete:'CASCADE'
    });
    Teacher.hasOne(models.Class,{
      foreignKey:'teacherId',
      as:'HeadTeacher'
    });
  };
  return Teacher;
};
