const express = require('express');
const logger = require('morgan');
const bodyParser = require('body-parser');
const multer = require('multer');
const randomstring = require('randomstring');
const fs   = require('fs');
const path = require('path');
const sharp = require('sharp');
const sizeOf = require('image-size');
const async = require('async');
//const redis = require('redis');
const nodemailer = require('nodemailer');
//const client = redis.createClient({return_buffers:true});
const moment = require('moment');

let transporter = nodemailer.createTransport({
  service:'gmail',
  host: 'smtp.gmail.com',
  auth:{
    user:"SimpleWebSpamEmail@gmail.com",
    pass:"Iamsosimple123"
  }
});

//client.on('connect', function() {
//    console.log('Redis client connected');
//});

//client.on('error', function (err) {
//    console.log('Something went wrong ' + err);
//});

// Set up the express app
const app = express();

var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'uploads')
  },
  filename: function (req, file, cb) {
    cb(null, file.fieldname + '-' + Date.now()+'-'+randomstring.generate(10))
  }
})

var upload = multer({ storage: storage })

// Log requests to the console.
app.use(logger('dev'));

// Parse incoming requests data (https://github.com/expressjs/body-parser)
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// Setup a default catch-all route that sends back a welcome message in JSON format.
//require('./server/routes')(app);

app.get("/",(req,res) => res.sendFile(__dirname+'/index.html'));

app.post("/upload/file",upload.single('myFile'),(req,res,next) =>{
  const file = req.file;
    if (!file) {
      const error = new Error('Please upload a file')
      error.httpStatusCode = 400
      return next(error)
    }
      res.send(file);
});
//image working
app.post("/upload/multiple",upload.array('myFiles'),(req,res,next) =>{
  const files = req.files
  if (!files) {
    const error = new Error('Please choose files')
    error.httpStatusCode = 400
    return next(error)
  }
  res.send(files);
  console.log(files);
});

let testdata;

app.post('/email',(req,res)=>{
  var mailOptions = {
    from:"SimpleWebSpamEmail@gmail.com",
    to:req.body.email,
    subject:"Test email",
    text:req.body.text
  };
  transporter.sendMail(mailOptions, function(error, info){
    if (error) {
      console.log(error);
    } else {
      console.log('Email sent:',info.response,moment().format("DD/MM/YYYY HH-mm-ss"));
      res.send("OK");
    }
  })
});

app.post("/file",async (req,res)=>{
  let height = req.body.height || 0;
  let width = req.body.width || 0;
  let filename = req.body.file+height+"x"+width;
  const dir = __dirname+'/uploads/'+req.body.file;
  let w,h;
  async.waterfall([
        (callback)=>
        {
          sizeOf(dir,(err, dimensions) => {
            callback(null,dimensions.width,dimensions.height);
          });
        }
        ,
        (w,h,callback) =>{
          sharp(dir)
          .resize(parseInt(req.body.height||h),parseInt(req.body.width||w))
          .rotate(90)
          .sharpen()
          .png()
          .toBuffer()
          .then( data => {
            res.setHeader('Content-Type', 'image/png');
            res.send(data);
          })
          .catch( (err) => callback(err));
        }],(err) => {
          if (err) {
            res.setHeader('Content-Type', 'text/plain');
            res.status(404).end('Not found');
          };
        });
});

//

module.exports = app;
